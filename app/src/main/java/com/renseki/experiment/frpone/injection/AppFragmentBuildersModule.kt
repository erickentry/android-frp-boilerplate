package com.renseki.experiment.frpone.injection

import com.renseki.experiment.frpone.demo.interactor.fragment.PasswordFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AppFragmentBuildersModule {
    @ContributesAndroidInjector
    abstract fun contributePasswordFragment(): PasswordFragment
}
