package com.renseki.experiment.frpone

import android.app.Activity
import android.app.Application
import com.renseki.experiment.frpone.injection.AppInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import timber.log.Timber
import javax.inject.Inject

/**
 * @author erick
 * @since 2018-09-26
 */
class App : Application(), HasActivityInjector {
    companion object {
        // It's for convenience. Though an app only has 1 instance of Application.
        lateinit var INSTANCE: App
    }

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun activityInjector() = dispatchingAndroidInjector

    override fun onCreate() {
        super.onCreate()

        // provide App instance
        INSTANCE = this

        // initialize Timber using trees that you've specified
        Timber.plant(Timber.DebugTree())

        // initialize Dagger to provide objects according to your scheme
        AppInjector.init(this)
    }
}