package com.renseki.experiment.frpone.demo.interactor

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.renseki.experiment.architecture.ext.setOnSafeClickListener
import com.renseki.experiment.architecture.util.autoCleared
import com.renseki.experiment.architecture.util.autoClearedLifeCycle
import com.renseki.experiment.frpone.R
import com.renseki.experiment.frpone.databinding.ActivityInteractorBinding
import com.renseki.experiment.frpone.demo.interactor.fragment.PasswordContract
import com.renseki.experiment.frpone.demo.interactor.fragment.PasswordFragment
import com.renseki.experiment.frpone.demo.interactor.fragment.PasswordInteractor
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class InteractorActivity : AppCompatActivity(), HasSupportFragmentInjector, InteractorContract.View {

    companion object {
        fun getStartIntent(context: Context) = Intent(context, InteractorActivity::class.java)
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    private var viewModel by autoCleared<InteractorContract.ViewModel>()
    private var passwordInteractor by autoCleared<PasswordContract.Interactor>()
    private var binding by autoClearedLifeCycle<ActivityInteractorBinding>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = getViewModelInstance()
        binding = getDataBindingInstance(viewModel)
        passwordInteractor = getPasswordInteractorInstance()
    }

    override fun getViewModelInstance(): InteractorContract.ViewModel {
        return ViewModelProviders
                .of(this, viewModelFactory)
                .get(InteractorViewModel::class.java)
                .apply {
                    getCloseDialogEvent().observe(this@InteractorActivity, Observer {
                        passwordInteractor.issueCommand(
                                PasswordContract.Interactor.Command.Close()
                        )
                    })
                }
    }

    override fun getPasswordInteractorInstance(): PasswordContract.Interactor {
        return ViewModelProviders
                .of(this, viewModelFactory)
                .get(PasswordInteractor::class.java)
                .apply {
                    getPasswordEvent().observe(this@InteractorActivity, Observer { password ->
                        if (password != null) {
                            viewModel.setMessage(
                                    "Inputted password: $password\n"
                            )

                            passwordInteractor.issueCommand(
                                    PasswordContract.Interactor.Command.ShowMessage(
                                            "Closing Dialog in: 3 seconds"
                                    )
                            )

                            viewModel.closeDialogAfter(3000)
                        }
                    })

                    getStateEvent().observe(this@InteractorActivity, Observer { state ->
                        if (state == PasswordContract.Interactor.State.DEAD) {
                            viewModel.cancelCloseDialogCommand()
                        }
                    })
                }
    }

    override fun getDataBindingInstance(viewModel: InteractorContract.ViewModel): ActivityInteractorBinding {
        return DataBindingUtil
                .setContentView<ActivityInteractorBinding>(this, R.layout.activity_interactor)
                .apply {
                    this.viewModel = viewModel

                    message.setOnSafeClickListener {
                        showPasswordDialog()
                    }
                }
    }

    override fun showPasswordDialog() {
        PasswordFragment
                .newInstance()
                .show(supportFragmentManager, getString(R.string.password_dialog))
    }
}
