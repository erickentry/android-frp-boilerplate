package com.renseki.experiment.frpone.demo.interactor.fragment

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.renseki.experiment.architecture.util.LiveEvent
import javax.inject.Inject

/**
 * @author erick
 * @since 2018-12-31
 */
class PasswordInteractor @Inject constructor() : ViewModel(), PasswordContract.Interactor {
    private val commandEvent = LiveEvent<PasswordContract.Interactor.Command>()
    private val passwordEvent = LiveEvent<String>()
    private val stateEvent = LiveEvent<PasswordContract.Interactor.State>()

    override fun getCommandEvent(): LiveData<PasswordContract.Interactor.Command> {
        return commandEvent
    }

    override fun issueCommand(command: PasswordContract.Interactor.Command) {
        commandEvent.value = command
    }

    override fun getPasswordEvent(): LiveData<String> {
        return passwordEvent
    }

    override fun setPassword(password: String) {
        passwordEvent.value = password
    }

    override fun setState(state: PasswordContract.Interactor.State) {
        stateEvent.value = state
    }

    override fun getStateEvent(): LiveData<PasswordContract.Interactor.State> {
        return stateEvent
    }
}