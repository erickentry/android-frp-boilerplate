package com.renseki.experiment.frpone.injection

import com.renseki.experiment.frpone.MainActivity
import com.renseki.experiment.frpone.demo.interactor.InteractorActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AppActivityModule {
    @ContributesAndroidInjector(modules = [AppFragmentBuildersModule::class])
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [AppFragmentBuildersModule::class])
    abstract fun contributeInteractorActivity(): InteractorActivity
}
