package com.renseki.experiment.frpone.demo.interactor.fragment

import android.app.Dialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.renseki.experiment.architecture.ext.setOnSafeClickListener
import com.renseki.experiment.architecture.injection.Injectable
import com.renseki.experiment.architecture.util.autoCleared
import com.renseki.experiment.architecture.util.autoClearedLifeCycle
import com.renseki.experiment.frpone.R
import com.renseki.experiment.frpone.databinding.FragmentPasswordBinding
import javax.inject.Inject

/**
 * @author erick
 * @since 2018-12-31
 */
class PasswordFragment : DialogFragment(), Injectable {
    companion object {
        fun newInstance() = PasswordFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private var binding by autoClearedLifeCycle<FragmentPasswordBinding>()
    private var interactor by autoCleared<PasswordInteractor>()
    private var viewModel by autoCleared<PasswordViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_password,
                container,
                false
        )
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders
                .of(this, viewModelFactory)
                .get(PasswordViewModel::class.java)

        interactor = ViewModelProviders
                .of(requireActivity(), viewModelFactory)
                .get(PasswordInteractor::class.java)
                .apply {
                    getCommandEvent().observe(this@PasswordFragment, Observer { command ->
                        when(command) {
                            is PasswordContract.Interactor.Command.ShowMessage -> {
                                viewModel.setMessage(command.message)
                            }
                            is PasswordContract.Interactor.Command.Close -> {
                                dismiss()
                            }
                        }
                    })

                    setState(PasswordContract.Interactor.State.ALIVE)
                }

        binding.viewModel = viewModel
        binding.submit.setOnSafeClickListener {
            viewModel.getPasswordStream().value?.also { password ->
                interactor.setPassword(password)
            }
        }
    }

    override fun onStop() {
        interactor.setState(PasswordContract.Interactor.State.DEAD)
        super.onStop()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
        }
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }
}