package com.renseki.experiment.frpone.injection

import android.app.Application
import com.renseki.experiment.architecture.injection.CommonModule
import com.renseki.experiment.frpone.App
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        CommonModule::class,
        AppModule::class,
        AppActivityModule::class
    ]
)
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: App)
}
