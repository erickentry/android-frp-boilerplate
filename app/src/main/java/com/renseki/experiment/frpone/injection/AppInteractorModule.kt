package com.renseki.experiment.frpone.injection

import android.arch.lifecycle.ViewModel
import com.renseki.experiment.architecture.injection.ViewModelKey
import com.renseki.experiment.frpone.demo.interactor.fragment.PasswordInteractor
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class AppInteractorModule {
    @Binds
    @IntoMap
    @ViewModelKey(PasswordInteractor::class)
    abstract fun bindPasswordInteractor(interactor: PasswordInteractor): ViewModel
}
