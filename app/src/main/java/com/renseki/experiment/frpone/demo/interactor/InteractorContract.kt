package com.renseki.experiment.frpone.demo.interactor

import android.arch.lifecycle.LiveData
import com.renseki.experiment.frpone.databinding.ActivityInteractorBinding
import com.renseki.experiment.frpone.demo.interactor.fragment.PasswordContract

/**
 * @author erick
 * @since 2018-12-31
 */
interface InteractorContract {
    interface ViewModel {
        fun getMessageStream(): LiveData<String>
        fun setMessage(message: String)
        fun getCloseDialogEvent(): LiveData<Unit>
        fun cancelCloseDialogCommand()
        fun closeDialogAfter(timeInMillis: Long)
    }

    interface View {
        fun getViewModelInstance(): ViewModel
        fun getPasswordInteractorInstance(): PasswordContract.Interactor
        fun getDataBindingInstance(viewModel: InteractorContract.ViewModel): ActivityInteractorBinding
        fun showPasswordDialog()
    }
}