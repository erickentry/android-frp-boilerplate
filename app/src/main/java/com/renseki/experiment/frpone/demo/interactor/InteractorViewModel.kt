package com.renseki.experiment.frpone.demo.interactor

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.renseki.experiment.architecture.util.LiveEvent
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * @author erick
 * @since 2018-12-31
 */
class InteractorViewModel @Inject constructor() : ViewModel(), InteractorContract.ViewModel {
    private val messageStream = MutableLiveData<String>().apply {
        value = "Click Here"
    }
    private var intervalObservable: Disposable? = null
    private val closeDialogEvent = LiveEvent<Unit>()

    override fun onCleared() {
        intervalObservable?.dispose()
        super.onCleared()
    }

    override fun getMessageStream(): LiveData<String> {
        return messageStream
    }

    override fun setMessage(message: String) {
        messageStream.value = message
    }

    override fun closeDialogAfter(timeInMillis: Long) {
        intervalObservable?.dispose()
        intervalObservable = Completable
                .create { emitter ->
                    try {
                        Thread.sleep(timeInMillis)
                    } catch (e: Exception) {
                        /* ignore */
                    } finally {
                        emitter.onComplete()
                    }
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    closeDialogEvent.value = Unit
                }
    }

    override fun getCloseDialogEvent(): LiveData<Unit> {
        return closeDialogEvent
    }

    override fun cancelCloseDialogCommand() {
        intervalObservable?.dispose()
    }
}