package com.renseki.experiment.frpone.injection

import android.arch.lifecycle.ViewModel
import com.renseki.experiment.architecture.injection.ViewModelKey
import com.renseki.experiment.frpone.demo.interactor.InteractorViewModel
import com.renseki.experiment.frpone.demo.interactor.fragment.PasswordViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class AppViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(InteractorViewModel::class)
    abstract fun bindInteractorViewModel(viewModel: InteractorViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PasswordViewModel::class)
    abstract fun bindPasswordViewModel(viewModel: PasswordViewModel): ViewModel
}
