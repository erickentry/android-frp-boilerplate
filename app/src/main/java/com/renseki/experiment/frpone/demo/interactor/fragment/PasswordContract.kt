package com.renseki.experiment.frpone.demo.interactor.fragment

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData

/**
 * @author erick
 * @since 2018-12-31
 */
interface PasswordContract {
    interface ViewModel {
        fun getPasswordStream(): MutableLiveData<String>
        fun getMessageStream(): LiveData<String>
        fun setMessage(message: String)
    }

    interface Interactor {
        interface Command {
            class ShowMessage(val message: String) : Command
            class Close : Command
        }

        enum class State {
            ALIVE, DEAD
        }

        fun getCommandEvent(): LiveData<Command>
        fun issueCommand(command: Command)
        fun getPasswordEvent(): LiveData<String>
        fun setPassword(password: String)
        fun setState(state: State)
        fun getStateEvent(): LiveData<State>
    }
}