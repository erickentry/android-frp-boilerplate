package com.renseki.experiment.frpone.injection

import dagger.Module

/**
 * Use this class to provide any object that you want to provide.
 * You might want to use @provide
 */
@Module(includes = [AppViewModelModule::class, AppInteractorModule::class])
class AppModule {

}
