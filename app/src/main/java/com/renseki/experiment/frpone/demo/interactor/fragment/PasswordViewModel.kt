package com.renseki.experiment.frpone.demo.interactor.fragment

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import javax.inject.Inject

/**
 * @author erick
 * @since 2018-12-31
 */
class PasswordViewModel @Inject constructor() : ViewModel(), PasswordContract.ViewModel {

    private val passwordStream = MutableLiveData<String>()
    private val messageStream = MutableLiveData<String>()

    override fun getPasswordStream(): MutableLiveData<String> {
        return passwordStream
    }

    override fun getMessageStream(): LiveData<String> {
        return messageStream
    }

    override fun setMessage(message: String) {
        messageStream.value = message
    }
}