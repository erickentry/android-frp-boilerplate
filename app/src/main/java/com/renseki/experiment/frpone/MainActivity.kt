package com.renseki.experiment.frpone

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.renseki.experiment.architecture.ext.setOnSafeClickListener
import com.renseki.experiment.architecture.util.autoCleared
import com.renseki.experiment.frpone.databinding.ActivityMainBinding
import com.renseki.experiment.frpone.demo.interactor.InteractorActivity
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class MainActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    private var binding by autoCleared<ActivityMainBinding>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil
                .setContentView<ActivityMainBinding>(this, R.layout.activity_main)
                .apply {
                    menuInteractor.setOnSafeClickListener {
                        startActivity(
                                InteractorActivity.getStartIntent(this@MainActivity)
                        )
                    }
                }
    }
}
