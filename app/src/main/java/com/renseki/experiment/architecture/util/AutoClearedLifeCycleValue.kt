package com.renseki.experiment.architecture.util

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.OnLifecycleEvent
import android.databinding.ViewDataBinding
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

/**
 * A lazy property that gets cleaned up when the lifecycleOwner is destroyed.
 * It also automatically registers lifecycle owner, and unregisters it onStop.
 *
 * Accessing this variable in a destroyed lifecycleOwner will throw NPE.
 */
class AutoClearedLifeCycleValue<T : ViewDataBinding>(lifecycleOwner: LifecycleOwner) : ReadWriteProperty<LifecycleOwner, T> {
    private var _value: T? = null

    init {
        lifecycleOwner.lifecycle.addObserver(object : LifecycleObserver {
            @OnLifecycleEvent(Lifecycle.Event.ON_START)
            fun onStart() {
                _value?.setLifecycleOwner(lifecycleOwner)
            }

            @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
            fun onStop() {
                _value?.setLifecycleOwner(null)
            }

            @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            fun onDestroy() {
                _value = null
            }
        })
    }

    override fun getValue(thisRef: LifecycleOwner, property: KProperty<*>): T {
        return _value ?: throw IllegalStateException(
            "should never call auto-cleared-value get when it might not be available"
        )
    }

    override fun setValue(thisRef: LifecycleOwner, property: KProperty<*>, value: T) {
        _value = value
    }
}

/**
 * Creates an [AutoClearedValue] associated with this lifecycleOwner.
 */
fun <T : ViewDataBinding> LifecycleOwner.autoClearedLifeCycle() = AutoClearedLifeCycleValue<T>(this)