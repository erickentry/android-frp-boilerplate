package com.renseki.experiment.architecture.ext



import com.renseki.experiment.architecture.model.Resource
import com.renseki.experiment.architecture.model.Status

/**
 * @author erick
 * @since 2018-07-25
 */
fun <T> Resource<T>?.successAvailable(callback: (T) -> Unit) {
    if (this != null && status == Status.SUCCESS && data != null) {
        callback.invoke(this.data)
    }
}

fun <T> Resource<T>?.available(callback: (T) -> Unit) {
    if (this != null && data != null) {
        callback.invoke(this.data)
    }
}

fun <T> Resource<T>?.error(callback: (String) -> Unit) {
    if (this != null && status == Status.ERROR && message != null) {
        callback.invoke(this.message)
    }
}