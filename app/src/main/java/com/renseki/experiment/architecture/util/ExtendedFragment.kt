package com.renseki.experiment.architecture.util

import android.content.Intent
import android.content.pm.PackageManager
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.util.SparseArray
import java.util.concurrent.atomic.AtomicInteger

/**
 * @author lukaskris
 * @since 2018/01/22
 */

@Suppress("unused")
open class ExtendedFragment : Fragment() {

    private val resultHandlers = SparseArray<ActivityHandler>()
    private val permissionHandlers = SparseArray<PermissionHandler>()
    private val counter = AtomicInteger(0)
    private val newRequestCode: Int
        get() = counter.incrementAndGet()

    protected fun startActivityForResult(intent: Intent, successResultCode: Int, handler: (data: Intent?) -> Unit) {
        val requestCode = newRequestCode
        resultHandlers.put(requestCode, ActivityHandler(successResultCode, handler))
        startActivityForResult(intent, requestCode)
    }

    protected fun startActivityForResult(intent: Intent, handler: (resultCode: Int, data: Intent?) -> Unit) {
        val requestCode = newRequestCode
        resultHandlers.put(requestCode, ActivityHandler(handler))
        startActivityForResult(intent, requestCode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        val handler = resultHandlers[requestCode]
        if (handler != null) {
            if (handler.successHandler != null) {
                if (resultCode == handler.successResultCode) {
                    handler.successHandler?.invoke(data)
                }
                return
            }
            if (handler.resultHandler != null) {
                handler.resultHandler?.invoke(resultCode, data)
            }
        }
    }

    protected fun checkPermission(permissions: Array<String>, handler: PermissionHandler) {
        val allPermissionsGranted = permissions.none { ContextCompat.checkSelfPermission(context!!, it) != PackageManager.PERMISSION_GRANTED }

        if (!allPermissionsGranted) {
            val requestCode = newRequestCode
            requestPermissions(permissions, requestCode)
            permissionHandlers.put(requestCode, handler)
        } else {
            handler.grantedHandler.invoke()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        val handler = permissionHandlers[requestCode]
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            handler?.grantedHandler?.invoke()
        } else {
            handler?.deniedHandler?.invoke()
        }
    }

    data class PermissionHandler(
        val grantedHandler: () -> Unit,
        val deniedHandler: (() -> Unit)?
    ) {
        constructor(grantedHandler: () -> Unit) : this(grantedHandler, null)
    }

    class ActivityHandler {
        var successResultCode: Int? = null
        var resultHandler: ((resultCode: Int, data: Intent?) -> Unit)? = null
        var successHandler: ((data: Intent?) -> Unit)? = null

        constructor(successResultCode: Int?, successHandler: (data: Intent?) -> Unit) {
            this.successResultCode = successResultCode
            this.successHandler = successHandler
        }

        constructor(resultHandler: (resultCode: Int, data: Intent?) -> Unit) {
            this.resultHandler = resultHandler
        }
    }
}