package com.renseki.experiment.architecture.ext

import java.util.concurrent.Callable

/**
 * @author erick
 * @since 2018-07-04
 */
fun List<Callable<Boolean>>.allValidationsPassed(): Boolean {
    forEach {
        if (!it.call()) return false
    }
    return true
}