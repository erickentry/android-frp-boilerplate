package com.renseki.experiment.architecture.ext

import io.reactivex.CompletableObserver
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable

/**
 * @author erick
 * @since 2018-07-05
 */
fun <T> observer(success: (result: T) -> Unit, error: (e: Throwable) -> Unit) = object: SingleObserver<T> {
    override fun onSuccess(t: T) {
        success.invoke(t)
    }

    override fun onError(e: Throwable) {
        error.invoke(e)
    }

    override fun onSubscribe(d: Disposable) {}
}

fun observer(success: () -> Unit, error: (e: Throwable) -> Unit) = object: CompletableObserver {
    override fun onComplete() {
        success.invoke()
    }

    override fun onError(e: Throwable) {
        error.invoke(e)
    }

    override fun onSubscribe(d: Disposable) {}
}