package com.renseki.experiment.architecture.ext

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.Transformations
import com.renseki.experiment.architecture.model.Resource
import com.renseki.experiment.architecture.util.AbsentLiveData
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

/**
 * @author erick
 * @since 2018-07-09
 */
fun <X, Y> LiveData<X>?.map(transform: (data: X) -> Y): LiveData<Y> {
    return if (this == null) AbsentLiveData.create()
    else Transformations.map(this) { data: X? ->
        if (data == null) null else transform.invoke(data)
    }
}

fun <X, Y> LiveData<X>?.switch(transform: (data: X) -> LiveData<Y>): LiveData<Y> {
    return if (this == null) AbsentLiveData.create()
    else Transformations.switchMap(this) { data: X? ->
        if (data == null) {
            AbsentLiveData.create()
        } else {
            transform.invoke(data)
        }
    }
}

fun <X> MediatorLiveData<X>.source(liveData: LiveData<X>) {
    addSource(liveData) { value = it }
}

@JvmName("sourceFromApiResponse")
fun <X> MediatorLiveData<Resource<X>>.source(liveData: LiveData<com.renseki.experiment.architecture.api.ApiResponse<X>>) {
    addSource(liveData) {
        value = when(it) {
            is com.renseki.experiment.architecture.api.ApiSuccessResponse -> Resource.success(it.body)
            is com.renseki.experiment.architecture.api.ApiErrorResponse -> Resource.error(it.errorMessage, null)
            else -> Resource.error("Empty response", null)
        }
    }
}

fun <T> LiveData<T>.blockingGet(): T {
    val data = arrayOfNulls<Any>(1)
    val latch = CountDownLatch(1)
    val observer = object : Observer<T> {
        override fun onChanged(o: T?) {
            data[0] = o
            latch.countDown()
            removeObserver(this)
        }
    }
    observeForever(observer)
    latch.await(2, TimeUnit.SECONDS)

    @Suppress("UNCHECKED_CAST")
    return data[0] as T
}

fun <A, B> LiveData<A>.combineLatest(b: LiveData<B>): LiveData<Pair<A, B>> {
    return MediatorLiveData<Pair<A, B>>().apply {
        var lastA: A? = null
        var lastB: B? = null

        addSource(this@combineLatest) {
            if (it == null && value != null) value = null
            lastA = it
            if (lastA != null && lastB != null) value = lastA!! to lastB!!
        }

        addSource(b) {
            if (it == null && value != null) value = null
            lastB = it
            if (lastA != null && lastB != null) value = lastA!! to lastB!!
        }
    }
}

fun <A, B, C> combineLatest(a: LiveData<A>, b: LiveData<B>, c: LiveData<C>): LiveData<Triple<A?, B?, C?>> {

    fun Triple<A?, B?, C?>?.copyWithFirst(first: A?): Triple<A?, B?, C?> {
        if (this@copyWithFirst == null) return Triple<A?, B?, C?>(first, null, null)
        return this@copyWithFirst.copy(first = first)
    }

    fun Triple<A?, B?, C?>?.copyWithSecond(second: B?): Triple<A?, B?, C?> {
        if (this@copyWithSecond == null) return Triple<A?, B?, C?>(null, second, null)
        return this@copyWithSecond.copy(second = second)
    }

    fun Triple<A?, B?, C?>?.copyWithThird(third: C?): Triple<A?, B?, C?> {
        if (this@copyWithThird == null) return Triple<A?, B?, C?>(null, null, third)
        return this@copyWithThird.copy(third = third)
    }

    return MediatorLiveData<Triple<A?, B?, C?>>().apply {
        addSource(a) { value = value.copyWithFirst(it) }
        addSource(b) { value = value.copyWithSecond(it) }
        addSource(c) { value = value.copyWithThird(it) }
    }
}