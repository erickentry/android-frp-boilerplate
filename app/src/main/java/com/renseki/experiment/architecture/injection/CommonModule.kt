package com.renseki.experiment.architecture.injection

import android.arch.lifecycle.ViewModelProvider
import com.renseki.experiment.architecture.viewmodel.ViewModelFactory
import dagger.Binds
import dagger.Module

@Module
abstract class CommonModule {
    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}
