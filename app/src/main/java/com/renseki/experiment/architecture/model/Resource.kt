package com.renseki.experiment.architecture.model

/**
 * A generic class that holds a value with its loading status.
 * @param <T>
</T> */
data class Resource<out T>(val status: Status, val data: T?, val message: String?) {
    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(Status.SUCCESS, data, null)
        }

        fun <T> error(msg: String, data: T?): Resource<T> {
            return Resource(Status.ERROR, data, msg)
        }

        fun <T> loading(data: T?): Resource<T> {
            return Resource(Status.LOADING, data, null)
        }
    }
}

fun <T> Resource<T>?.successAvailable(callback: (T) -> Unit) {
    if (this != null && status == Status.SUCCESS && data != null) {
        callback.invoke(this.data)
    }
}

fun <T> Resource<T>?.available(callback: (T) -> Unit) {
    if (this != null && data != null) {
        callback.invoke(this.data)
    }
}

fun <T> Resource<T>?.error(callback: (String) -> Unit) {
    if (this != null && status == Status.ERROR && message != null) {
        callback.invoke(this.message)
    }
}
