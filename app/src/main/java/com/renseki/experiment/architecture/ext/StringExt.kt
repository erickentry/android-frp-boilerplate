package com.renseki.experiment.architecture.ext

import java.math.BigDecimal
import java.net.URLEncoder

/**
 * @author erick
 * @since 2018-07-03
 */
fun String.toIndonesianPhone() = "+62${removeInsignificantPhoneCharacter().replace("^0|\\+?62".toRegex(), "")}"

fun String.isPhone() = removeInsignificantPhoneCharacter().replace("[\\d|+]".toRegex(), "").isEmpty()

fun String.removeInsignificantPhoneCharacter() = replace("[ #*.,();\\-]".toRegex(), "")

fun String.urlEncode() = URLEncoder.encode(this, "UTF-8")

fun String.toBigDecimal() = BigDecimal(replace(".", ""))