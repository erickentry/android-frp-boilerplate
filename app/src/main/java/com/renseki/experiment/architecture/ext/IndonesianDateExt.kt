package com.renseki.experiment.architecture.ext

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

/**
 * @author erick
 * @since 2018-07-02
 */

@SuppressLint("SimpleDateFormat")
val simpleDateFormat = SimpleDateFormat()

const val DATE_TIME = "yyyy-MM-dd HH:mm:ss"
const val FULL_DATE = "EEEE, dd MMMM yyyy"

fun String.date(pattern: String = DATE_TIME): Date {
    return simpleDateFormat.let { format ->
        format.applyPattern(pattern)
        format.parse(this@date)
    }
}

fun Date.string(pattern: String = DATE_TIME): String {
    return simpleDateFormat.let { format ->
        format.applyPattern(pattern)
        format.format(this@string)
    }
}