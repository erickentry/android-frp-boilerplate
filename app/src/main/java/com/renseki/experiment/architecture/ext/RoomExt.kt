package com.renseki.experiment.architecture.ext

/**
 * @author erick
 * @since 2018-08-24
 */
fun <E> List<E>.asWhereInQuery(field: String): String? {
    return if (isEmpty()) {
        null
    } else {
        "($field in (${joinToString(",")})) and "
    }
}