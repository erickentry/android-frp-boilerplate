package com.renseki.experiment.architecture.ext

import android.database.Cursor

/**
 * @author erick
 * @since 2018-09-25
 */
fun Cursor.getInt(fieldName: String) = getInt(getColumnIndex(fieldName))
fun Cursor.getString(fieldName: String): String = getString(getColumnIndex(fieldName))
fun Cursor.getNullableString(fieldName: String): String? = if (isNull(fieldName)) null else getString(fieldName)
fun Cursor.isNull(fieldName: String) = isNull(getColumnIndex(fieldName))