package com.renseki.experiment.architecture.binding.adapter

import android.databinding.BindingAdapter
import android.view.View

/**
 * @author erick
 * @since 2018-12-31
 */
object ViewBindingAdapter {
    @BindingAdapter("visibleGone")
    @JvmStatic
    fun setVisibleOrGone(view: View, isVisible: Boolean) {
        view.visibility = if (isVisible) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }
}