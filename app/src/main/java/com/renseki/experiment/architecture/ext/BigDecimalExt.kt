package com.renseki.experiment.architecture.ext

import java.math.BigDecimal
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

/**
 * @author erick
 * @since 2018-07-12
 */
fun BigDecimal?.toIndonesian(): String {
    return (NumberFormat.getInstance(Locale("in", "id")) as DecimalFormat).format(this ?: BigDecimal.ZERO)
}

fun BigDecimal?.rupiah(): String {
    return "Rp ${this?.toIndonesian() ?: 0}"
}

fun BigDecimal?.isZero(): Boolean{
    return this == BigDecimal.ZERO
}