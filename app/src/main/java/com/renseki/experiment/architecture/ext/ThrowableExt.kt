package com.renseki.experiment.architecture.ext

import java.io.PrintWriter
import java.io.StringWriter

/**
 * @author erick
 * @since 2018-07-03
 */
fun Throwable.string(): String {
    return StringWriter().use { stringWriter ->
        PrintWriter(stringWriter).use { printWriter ->
            printStackTrace(printWriter)
            stringWriter.toString()
        }
    }
}