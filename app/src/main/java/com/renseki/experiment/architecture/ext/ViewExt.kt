package com.renseki.experiment.architecture.ext

import android.os.SystemClock
import android.view.View

/**
 * @author erick
 * @since 2018-12-31
 */
fun View.setOnSafeClickListener(handler: (View) -> Unit) {
    setOnClickListener {
        if (SystemClock.elapsedRealtime() - (it.tag as Long? ?: 0) >= 500){
            handler.invoke(this)
            it.tag = SystemClock.elapsedRealtime()
        }
    }
}