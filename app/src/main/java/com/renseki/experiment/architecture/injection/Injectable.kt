package com.renseki.experiment.architecture.injection

/**
 * Marks an activity / lifecycleOwner injectable.
 */
interface Injectable
